using AlexKadyrbayev.Plugins.DataHandler;
using UnityEngine.UI;

namespace CharacterTalents.PlayerPoints
{
    public class PlayerPointsCountObserver
    {
        private IPlayerPointsPresenter m_Presenter;
        private IDataHandler<PlayerPointsData> m_PlayerPointsDataHandler;
        private Text m_PointsText;

        public PlayerPointsCountObserver(
            IPlayerPointsPresenter playerPointsPresenter,
            IDataHandler<PlayerPointsData> playerPointsDataHandler,
            Text pointsText)
        {
            m_Presenter = playerPointsPresenter;
            m_PlayerPointsDataHandler = playerPointsDataHandler;
            m_PointsText = pointsText;
        }

        public void Initialize()
        {
            m_PointsText.text = m_PlayerPointsDataHandler.Data.Count.ToString();
            m_Presenter.OnValueChanged += ChangeTextCount;
        }

        private void ChangeTextCount()
        {
            m_PointsText.text = m_PlayerPointsDataHandler.Data.Count.ToString();
        }
    }
}