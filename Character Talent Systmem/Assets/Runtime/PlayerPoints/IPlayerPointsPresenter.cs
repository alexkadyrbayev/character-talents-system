using System;

namespace CharacterTalents.PlayerPoints
{
    public interface IPlayerPointsPresenter
    {
        public void Configure(PlayerPointsPresenterConfig config);
        public void EarnPoints(int count);
        public void RemovePoints(int count);
        public int PointsCount { get; }

        public event Action OnValueChanged;
    }
}