using AlexKadyrbayev.Plugins.DataHandler;
using UnityEngine;

namespace CharacterTalents.PlayerPoints
{
    [CreateAssetMenu(menuName = "Dev/DataHandler/PlayerPointsDataHandler", fileName = "PlayerPointsDataHandler")]
    public class PlayerPointsDataHandler : DataHandler<PlayerPointsData> { }
}