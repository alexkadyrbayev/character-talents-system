using AlexKadyrbayev.Plugins.DataHandler;
using AlexKadyrbayev.Plugins.DataManagement;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace CharacterTalents.PlayerPoints
{
    public class PlayerPointsWizard : MonoBehaviour
    {
        [SerializeField] private Button m_PointsButton;
        [SerializeField] private Text m_PlayerPointsCountView;

        [Inject] private IDataHandlersRegistry m_DataHandlersRegistry;
        [Inject] private IPlayerPointsPresenter m_PlayerPointsPresenter;

        private IDataHandler<PlayerPointsData> m_PlayerPointsDataHandler;

        private const string m_DataHandlerKey = "player_points_data";
        private const int m_AddingPointsCount = 1000;

        public void Awake()
        {
            m_PointsButton.onClick.AddListener(OnButtonClick);

            Initialize();
        }

        private void Initialize()
        {
            m_PlayerPointsDataHandler = m_DataHandlersRegistry.
                GetDataHandlerObject<PlayerPointsData>(m_DataHandlerKey);

            m_PlayerPointsPresenter.Configure(new PlayerPointsPresenterConfig()
            {
                PlayerPointsDataHandler = m_PlayerPointsDataHandler,
                PlayerPointsCountView = m_PlayerPointsCountView
            });
        }

        private void OnButtonClick()
        {
            m_PlayerPointsPresenter.EarnPoints(m_AddingPointsCount);
        }
    }
}