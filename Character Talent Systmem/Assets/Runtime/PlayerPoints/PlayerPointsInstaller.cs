using Zenject;

namespace CharacterTalents.PlayerPoints
{
    public class PlayerPointsInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container
                .BindInterfacesTo<PlayerPointsPresenter>()
                .AsSingle()
                .NonLazy();
        }
    }
}