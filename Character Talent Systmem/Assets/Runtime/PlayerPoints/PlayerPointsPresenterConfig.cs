using AlexKadyrbayev.Plugins.DataHandler;
using UnityEngine.UI;

namespace CharacterTalents.PlayerPoints
{
    public class PlayerPointsPresenterConfig
    {
        public IDataHandler<PlayerPointsData> PlayerPointsDataHandler { get; set; }
        public Text PlayerPointsCountView { get; set; }
    }
}