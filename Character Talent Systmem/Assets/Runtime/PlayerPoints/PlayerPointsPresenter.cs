using AlexKadyrbayev.Plugins.DataHandler;
using System;

namespace CharacterTalents.PlayerPoints
{
    public class PlayerPointsPresenter : IPlayerPointsPresenter
    {
        private PlayerPointsCountObserver m_PlayerPointsCountObserver;
        private IDataHandler<PlayerPointsData> m_PlayerPointsDataHandler;

        public int PointsCount => m_PlayerPointsDataHandler.Data.Count;

        public event Action OnValueChanged;

        public void Configure(PlayerPointsPresenterConfig config)
        {
            m_PlayerPointsDataHandler = config.PlayerPointsDataHandler;

            m_PlayerPointsCountObserver = new PlayerPointsCountObserver(
                this,
                config.PlayerPointsDataHandler,
                config.PlayerPointsCountView);

            m_PlayerPointsCountObserver.Initialize();
        }

        public void EarnPoints(int count)
        {
            m_PlayerPointsDataHandler.Data.EarnPoints(count);

            m_PlayerPointsDataHandler.Save();

            OnValueChanged?.Invoke();
        }

        public void RemovePoints(int count)
        {
            m_PlayerPointsDataHandler.Data.RemovePoints(count);

            m_PlayerPointsDataHandler.Save();

            OnValueChanged?.Invoke();
        }
    }
}