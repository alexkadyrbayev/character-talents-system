using Newtonsoft.Json;
using System;
using UnityEngine;

namespace CharacterTalents.PlayerPoints
{
    [Serializable, JsonObject(MemberSerialization.Fields)]
    public class PlayerPointsData
    {
        [SerializeField] private int m_Count;
        public int Count => m_Count;

        public void EarnPoints(int count)
        {
            m_Count += count;
        }

        public void RemovePoints(int count)
        {
            m_Count -= count;
        }
    }
}