using Newtonsoft.Json.Linq;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    /// <summary>
    /// ��������� ������
    /// </summary>
    public interface IDataLoader
    {
        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="onSaveLoaded">callback ����� ���������� ��������</param>
        /// <param name="onLoaded"></param>
        /// <returns></returns>
        void Load(System.Action<JObject, bool> onLoaded);

        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="jObj">data</param>
        /// <param name="onCompleted">callback ����� ���������� ����������</param>
        /// <returns></returns>
        void Save(JObject jObj, System.Action<bool> onCompleted);

    }
}