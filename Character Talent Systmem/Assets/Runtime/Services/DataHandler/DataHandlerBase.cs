﻿using UnityEngine;

namespace AlexKadyrbayev.Plugins.DataHandler
{
    public abstract class DataHandlerBase : ScriptableObject, IDataHandler
    {
        public abstract bool HasData { get; }
        public abstract void Save();
        public abstract bool Clear();
        public abstract string GetDataKey();
    }
}