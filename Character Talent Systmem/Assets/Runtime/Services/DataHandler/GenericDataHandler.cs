using System;
using UnityEngine;

namespace AlexKadyrbayev.Plugins.DataHandler
{
    public abstract class GenericDataHandler<T> : DataHandlerBase, IDataHandler<T>
    {
        [NonSerialized]
        protected T _data;
        public override bool HasData => _data != null;
        public Action<T> OnDataChanged { get; set; }
        public abstract T Data { get; }
        public virtual Type DataType => typeof(T);
        public virtual T CreateDefault() => (T)Activator.CreateInstance(DataType);
        public abstract bool Load(out T data);
        public abstract void Save(T data);
        public override void Save() => Save(_data);
    }
}