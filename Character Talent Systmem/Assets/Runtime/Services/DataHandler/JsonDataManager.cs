using Newtonsoft.Json.Linq;
using System;
using UnityEngine;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public class JsonDataManager : IDataManager
    {
        public const string NameDataFile = "SaveData";
        private JObjectData m_DataController;
        private IDataLoader m_FileDataLoader;
        private ActionDistributor m_SaveActionDistributor;

        private static JsonDataManager m_Instance;
        public static IDataManager Instance => m_Instance ??= new JsonDataManager();

        public event Action<string, object> OnDataSaved;
        public Action OnDataChanged { get; set; } = () => { };

        public Action OnDataLoaded { get; set; } = () => { };
        public bool IsDataLoaded { get; private set; } = false;

        private JsonDataManager()
        {
            m_FileDataLoader = new FileDataLoader(NameDataFile);
            m_DataController = new JObjectData();
            m_SaveActionDistributor = new ActionDistributor();
            m_SaveActionDistributor.DefineExecutableAction(WriteFile);
            m_SaveActionDistributor.DefineMarginalDelayBetweenActions(TimeSpan.FromSeconds(1));
            m_SaveActionDistributor.OnExecutionCompleted += OnSaveExecutionCompleted;
        }

        private static void OnSaveExecutionCompleted(int count)
        {
            // DebugLogger.Log($"[Data Manager]: Execution completed! Blocked actions count: '{count}'");
        }

        /// <summary>
        /// �������� ����� �� �����, ���� ��� ���, ������ �����
        /// </summary>
        public void Load()
        {
            if (IsDataLoaded)
            {
                return;
            }

            m_FileDataLoader.Load(
                (jObj, result) =>
                {
                    if (result)
                    {
                        m_DataController.AddAllData(jObj);
                    }
                    else
                    {
                        SaveJson();
                    }

                    IsDataLoaded = true;
                    OnDataLoaded.Invoke();
                });
        }

        /// <summary>
        /// ��������� �� �������
        /// </summary>
        /// <param name="onDataSent"></param>
        public void SaveServer(Action<bool> onDataSent = null)
        {
            WriteUserData();
            //m_ServerDataLoader.Save(m_DataController.Data, onDataSent);
        }

        public bool TrySaveData<T>(string key, T data)
        {
            return TrySaveData(key, (object)data);
        }

        public bool TrySaveData(string key, object data)
        {
            OnDataSaved?.Invoke(key, data);
            if (!IsDataLoaded)
            {
                return false;
            }

            try
            {
                JObject jToken = JObject.FromObject(data);
                m_DataController.AddData(key, jToken);

                SaveJson();

                OnDataChanged.Invoke();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public bool TryGetData<T>(string key, out T data)
        {
            if (TryGetData(key, typeof(T), out var dataObject) && dataObject is T tData)
            {
                data = tData;
                return true;
            }

            data = default;
            return false;
        }

        public bool TryGetData(string key, Type type, out object data)
        {
            if (!IsDataLoaded)
            {
                data = null;
                return false;
            }

            JToken jToken = m_DataController.GetJToken(key);
            if (jToken != null)
            {
                data = jToken.ToObject(type);
                return true;
            }

            data = default;
            return false;
        }

        public bool TryRemoveData(string key)
        {
            bool result = m_DataController.Remove(key);
            if (result)
            {
                SaveJson();
                OnDataChanged.Invoke();
            }

            return result;
        }

        public bool HasKey(string key)
        {
            return m_DataController.HasKey(key);
        }

        /// <summary>
        /// ��������� � ������� � ����
        /// </summary>
        private void SaveJson()
        {
            m_SaveActionDistributor.Execute();
        }

        private void WriteUserData()
        {
            m_SaveActionDistributor.ExecuteForced();
        }

        private void WriteFile()
        {
            m_FileDataLoader.Save(m_DataController.Data, result =>
            {
                Debug.Log($"[Data Manager]: File writing completed with result: '{result}';");
            });
        }

        ~JsonDataManager()
        {
            m_SaveActionDistributor.OnExecutionCompleted -= OnSaveExecutionCompleted;
        }
    }
}