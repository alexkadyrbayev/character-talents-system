using System;

namespace AlexKadyrbayev.Plugins.DataHandler
{
    public interface IDataHandler
    {
        void Save();
    }

    public interface IDataHandler<T> : IDataHandler
    {
        Action<T> OnDataChanged { get; set; }
        T Data { get; }
        T CreateDefault();
        void Save(T data);
        bool Clear();
        bool HasData { get; }
    }
}
