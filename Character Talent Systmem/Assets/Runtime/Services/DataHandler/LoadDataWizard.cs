using UnityEngine;
using System;

namespace AlexKadyrbayev.Plugins.DataManagement
{
    public class LoadDataWizard : MonoBehaviour
    {
        private FileDataLoader m_FileDataLoader;

        public event Action OnLoaded = delegate { };
        public bool IsLoaded { get; private set; }

        public void Awake()
        {
            m_FileDataLoader = new FileDataLoader(JsonDataManager.NameDataFile);
            IsLoaded = false;
            LoadData();
        }

        private void LoadData()
        {
            JsonDataManager.Instance.OnDataLoaded += OnLoad;
            JsonDataManager.Instance.Load();
        }

        private void OnLoad()
        {
            JsonDataManager.Instance.OnDataLoaded -= OnLoad;
            OnLoaded();
            IsLoaded = true;
        }
    }
}