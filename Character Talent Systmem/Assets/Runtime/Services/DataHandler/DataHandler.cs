using AlexKadyrbayev.Plugins.DataManagement;
using System.Collections.Generic;
using UnityEngine;

namespace AlexKadyrbayev.Plugins.DataHandler
{
    public class DataHandler<T> : GenericDataHandler<T>
    {
        [SerializeField]
        private string m_Key;
        public override string GetDataKey() => m_Key;
        public override bool HasData => DataManager.HasKey(GetDataKey());
        private IDataManager DataManager
            => JsonDataManager.Instance;
        public override T Data
        {
            get
            {
                if (!DataManager.HasKey(GetDataKey()))
                {
                    DataManager.TrySaveData(GetDataKey(), _data = CreateDefault());
                }
                if (DataManager.HasKey(GetDataKey()) && _data == null)
                {
                    Load(out _data);
                }
                if (_data == null)
                {
                    _data = CreateDefault();
                }
                return _data;
            }
        }
        public override bool Load(out T data)
        {
            DataManager.TryGetData(GetDataKey(), DataType, out var obj);
            if (obj is T typed)
            {
                return (_data = data = typed) != null;
            }
            data = default;
            return false;
        }
        public override void Save(T data)
        {
            _data = data;
            DataManager.TrySaveData(GetDataKey(), data);
            OnDataChanged?.Invoke(data);
        }
        public override bool Clear()
        {
            bool result = DataManager.TryRemoveData(GetDataKey());
            if (result)
            {
                Load(out _data);
            }
            return result;
        }
        private void OnEnable()
        {
            if (DataManager == null)
            {
                return;
            }
            DataManager.OnDataLoaded += Initialize;
            DataManager.OnDataSaved += OnDataUpdated;
            if (DataManager.IsDataLoaded)
            {
                Initialize();
            }
        }
        private void OnDisable()
        {
            if (DataManager == null)
            {
                return;
            }
            DataManager.OnDataLoaded -= Initialize;
            DataManager.OnDataSaved -= OnDataUpdated;
        }
        private void OnDataUpdated(string key, object data)
        {
            if (key != GetDataKey())
            {
                return;
            }
            if (data is T castedData)
            {
                _data = castedData;
            }
        }
        public static TDataHandler Create<TDataHandler>(string dataKey)
            where TDataHandler : DataHandler<T>
        {
            var dataHandler = CreateInstance<TDataHandler>();
            dataHandler.m_Key = dataKey;
            return dataHandler;
        }
        protected virtual void Initialize() { }
    }
}