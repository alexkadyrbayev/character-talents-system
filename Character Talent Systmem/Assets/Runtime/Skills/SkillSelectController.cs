using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CharacterTalents.Skills
{
    public class SkillSelectController : MonoBehaviour
    {
        [Inject] private PanAndZoomCamera m_Camera;

        [SerializeField] private float m_ScaleFactor = 0.3f;

        private List<SkillComponent> m_AllSkills;

        public static SkillSelectController Instance { get; private set; }

        public void Start()
        {
            Input.multiTouchEnabled = false;

            if (Instance != null)
            {
                return;
            }

            Instance = this;
        }

        public void Initialize(List<SkillComponent> allSkills)
        {
            m_AllSkills = allSkills;

            UnlockOpportunitySelectSkills();
        }

        public void SelectSkill(SkillView skillComponent)
        {
            m_Camera.Lock();

            SelectAnimation(skillComponent).Play();

            skillComponent.SetButtonsActive(true);

            m_Camera.TweenedMoveTo(
                skillComponent.transform.position,
                10f,
                null,
                true);

            LockOpportunitySelectAnotherSkills(skillComponent);
        }

        private void LockOpportunitySelectAnotherSkills(SkillView skillComponent)
        {
            var allSkillsView = new List<SkillView>();

            foreach (var skill in m_AllSkills)
            {
                allSkillsView.Add(skill.View);
            }

            allSkillsView.Remove(skillComponent);

            foreach (var skillView in allSkillsView)
            {
                skillView.SetCanClickOnSkill(false);
            }
        }

        private void UnlockOpportunitySelectSkills()
        {
            foreach (var skill in m_AllSkills)
            {
                skill.SetCanClickOnSkill(true);
            }
        }

        private Tween SelectAnimation(SkillView skillComponent)
        {
            var startScale = skillComponent.StartScale;

            var tween = skillComponent.transform
                .DOScale(new Vector3(startScale.x + m_ScaleFactor, startScale.y + m_ScaleFactor), 0.2f)
                .SetEase(Ease.Linear);

            return tween;
        }

        private Tween DeselectAnimation(SkillView skillComponent)
        {
            var startScale = skillComponent.StartScale;

            var tween = skillComponent.transform
                .DOScale(new Vector3(startScale.x, startScale.y), 0.2f)
                .SetEase(Ease.Linear);

            return tween;
        }

        public void DeselectSkill(SkillView skillComponent)
        {
            DeselectAnimation(skillComponent);

            skillComponent.SetButtonsActive(false);

            m_Camera.UnLock();

            UnlockOpportunitySelectSkills();
        }
    }
}