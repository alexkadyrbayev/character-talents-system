using Sirenix.Utilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterTalents.Skills
{
    public class SkillComponent : MonoBehaviour
    {
        [SerializeField] private SkillType m_SkillType;
        [SerializeField] private int m_SkillCost;
        [SerializeField] private SkillView m_View;
        [SerializeField] private List<SkillComponent> m_SkillDependentsToLearn;
        [SerializeField] private List<SkillComponent> m_SkillDependentsToForget;

        private bool m_IsLearned;

        public int Price => m_SkillCost;
        public SkillView View => m_View;
        public bool IsLearned => m_IsLearned;

        public Action<SkillComponent> OnLearnButtonClick;
        public Action<SkillComponent> OnForgetButtonClick;

        public event Action OnSkillLearned;
        public event Action OnSkillForgot;

        private void OnEnable()
        {
            if (IsBaseSkill())
            {
                m_IsLearned = true;
                m_View.SetCanClickOnSkill(false);
                m_View.SetButtonsActive(false);
                m_View.OnSkillLearned();
            }

            Initialize();

            m_View.OnLearnClick += OnLearnClick;
            m_View.OnForgetClick += OnForgetClick;
        }

        private void Initialize()
        {
            m_View.SetPrice(m_SkillCost);
        }

        private void OnLearnClick()
        {
            OnLearnButtonClick?.Invoke(this);
        }

        private void OnForgetClick()
        {
            OnForgetButtonClick?.Invoke(this);
        }

        public void SetCanSkillLearn(bool isEnoughPoints)
        {
            if (IsBaseSkill())
            {
                return;
            }
            if (!m_IsLearned)
            {
                m_View.SetCanLearnSkill(isEnoughPoints && IsCanLearn());
            }
        }

        public void SetCanForgetSkill()
        {
            if (IsBaseSkill())
            {
                return;
            }
            m_View.SetCanForgetSkill(IsCanForget());
        }

        public void LearnSkill()
        {
            var skillType = m_SkillCost;
            m_IsLearned = true;
            m_View.OnSkillLearned();

            OnSkillLearned?.Invoke();
        }

        public void ForgetSkill()
        {
            m_IsLearned = false;
            m_View.OnSkillForgot();

            OnSkillForgot?.Invoke();
        }

        private bool IsCanForget()
        {
            if (IsBaseSkill())
            {
                return false;
            }

            foreach (var skill in m_SkillDependentsToForget)
            {
                if (skill.IsLearned)
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsCanLearn()
        {
            if (IsBaseSkill())
            {
                return false;
            }

            foreach (var skill in m_SkillDependentsToLearn)
            {
                if (skill.IsLearned)
                {
                    return true;
                }
            }

            return false;
        }

        public void SetCanClickOnSkill(bool isCanClick)
        {
            if (IsBaseSkill())
            {
                return;
            }
            m_View.SetCanClickOnSkill(isCanClick);
        }

        private bool IsBaseSkill()
        {
            return m_SkillDependentsToLearn.IsNullOrEmpty();
        }

        public void OnDestroy()
        {
            m_View.OnLearnClick -= OnLearnClick;
            m_View.OnForgetClick -= OnForgetClick;
        }
    }
}