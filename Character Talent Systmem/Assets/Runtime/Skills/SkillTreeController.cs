using CharacterTalents.PlayerPoints;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace CharacterTalents.Skills
{
    public class SkillTreeController : MonoBehaviour
    {
        [SerializeField] private List<SkillComponent> m_AllSkills;
        [SerializeField] private Button m_ForgetAllButton;
        [SerializeField] private SkillSelectController m_SkillSelectController;

        [Inject] private IPlayerPointsPresenter m_PointsPresenter;

        private List<SkillComponent> m_LearnedSkills = new List<SkillComponent>();

        public void Start()
        {
            m_SkillSelectController.Initialize(m_AllSkills);

            Initialize();
        }

        private void Initialize()
        {
            m_ForgetAllButton.onClick.AddListener(ForgetAll);

            foreach (var skill in m_AllSkills)
            {
                skill.SetCanSkillLearn(IsEnoughPointsToLearn(skill));
                skill.OnLearnButtonClick += OnLearnSkillButtonClick;
                skill.OnForgetButtonClick += OnForgetSkillButtonClick;
                skill.OnSkillLearned += UpdateSkillViews;
                skill.OnSkillForgot += UpdateSkillViews;
            }

            m_PointsPresenter.OnValueChanged += UpdateSkillViews;
        }

        private void UpdateSkillViews()
        {
            foreach (var skill in m_AllSkills)
            {
                skill.SetCanSkillLearn(IsEnoughPointsToLearn(skill));
                skill.SetCanForgetSkill();
            }
        }

        private void OnLearnSkillButtonClick(SkillComponent skill)
        {
            skill.LearnSkill();
            m_LearnedSkills.Add(skill);
            m_PointsPresenter.RemovePoints(skill.Price);
        }

        private void OnForgetSkillButtonClick(SkillComponent skill)
        {
            m_PointsPresenter.EarnPoints(skill.Price);
            skill.ForgetSkill();
            m_LearnedSkills.Remove(skill);
        }

        private bool IsEnoughPointsToLearn(SkillComponent skill)
        {
            if (m_PointsPresenter.PointsCount >= skill.Price)
            {
                return true;
            }

            return false;
        }

        private void ForgetAll()
        {
            foreach (var skill in m_LearnedSkills)
            {
                skill.ForgetSkill();
                m_PointsPresenter.EarnPoints(skill.Price);
            }
            m_LearnedSkills = new List<SkillComponent>();

            UpdateSkillViews();
        }

        public void OnDestroy()
        {
            m_ForgetAllButton.onClick.RemoveListener(ForgetAll);

            foreach (var skill in m_AllSkills)
            {
                skill.OnLearnButtonClick -= OnLearnSkillButtonClick;
                skill.OnForgetButtonClick -= OnForgetSkillButtonClick;
                skill.OnSkillLearned -= UpdateSkillViews;
                skill.OnSkillForgot -= UpdateSkillViews;
            }
        }
    }
}