using AlexKadyrbayev.Utilities;
using System;
using TMPro;
using UnityEngine;

namespace CharacterTalents.Skills
{
    public class SkillView : MonoBehaviour
    {
        [SerializeField] private GameObject m_PricePlaceholder;
        [SerializeField] private TextMeshPro m_PriceView;
        [SerializeField] private ColliderButton m_LearnButton;
        [SerializeField] private ColliderButton m_CancelButton;
        [SerializeField] private SpriteRenderer m_SpriteRenderer;
        [SerializeField] private Color m_CanLearnSkillColor;
        [SerializeField] private Color m_CantLearnSkillColor;
        [SerializeField] private ColliderButton m_ForgetButton;

        private bool m_IsInteractable;
        private bool m_IsLearned;

        private Vector3 m_StartScale;
        public Vector3 StartScale => m_StartScale;

        public Action OnLearnClick;
        public Action OnForgetClick;

        public void OnEnable()
        {
            SetButtonsActive(false);
            m_LearnButton.OnClick += OnLearnButtonClick;
            m_CancelButton.OnClick += Deselect;
            m_StartScale = gameObject.transform.localScale;
            m_ForgetButton.OnClick += OnForgetButtonClick;
        }

        private void Deselect()
        {
            SkillSelectController.Instance.DeselectSkill(this);
        }

        private void OnLearnButtonClick()
        {
            OnLearnClick?.Invoke();
        }

        private void OnMouseDown()
        {
            if (!m_IsInteractable)
            {
                return;
            }

            SkillSelectController.Instance.SelectSkill(this);
        }

        public void SetCanClickOnSkill(bool isCanClick)
        {
            m_IsInteractable = isCanClick;
        }

        public void SetCanLearnSkill(bool isCanLearn)
        {
            m_SpriteRenderer.color = isCanLearn ? m_CanLearnSkillColor : m_CantLearnSkillColor;

            m_LearnButton.SetInteractable(isCanLearn);
        }

        public void SetCanForgetSkill(bool isCanForget)
        {
            m_ForgetButton.SetInteractable(isCanForget);
        }

        public void OnDestroy()
        {
            m_LearnButton.OnClick -= OnLearnButtonClick;
            m_CancelButton.OnClick -= Deselect;
            m_ForgetButton.OnClick -= OnForgetButtonClick;
        }

        public void SetButtonsActive(bool isActive)
        {
            m_ForgetButton.SetEnable(isActive && m_IsLearned);
            m_LearnButton.SetEnable(isActive && !m_IsLearned);
            m_CancelButton.SetEnable(isActive);
        }

        public void SetPrice(int price)
        {
            m_PriceView.text = price.ToString();
        }

        private void OnForgetButtonClick()
        {
            OnForgetClick?.Invoke();
        }

        public void OnSkillLearned()
        {
            m_IsLearned = true;
            m_SpriteRenderer.color = Color.white;
            m_PricePlaceholder.SetActive(false);
            m_ForgetButton.SetEnable(true);
            m_LearnButton.SetEnable(false);
        }

        public void OnSkillForgot()
        {
            m_IsLearned = false;
            m_PricePlaceholder.SetActive(true);
            m_ForgetButton.SetEnable(false);
            m_LearnButton.SetEnable(true);
        }
    }
}