using System.IO;
using UnityEditor;
using UnityEngine;

namespace AlexKadyrbayev.Plugins.Utilites
{
    public static class GameDataExtensions
    {
        [MenuItem("Dev/Data/Clean All Data", priority = 0)]
        private static void CleanAllData()
        {
            CleanAppData();
            CleanPlayerPrefs();
        }

        [MenuItem("Dev/Data/Clean Application Data", priority = 11)]
        private static void CleanAppData()
        {
            DirectoryInfo dataDir = new DirectoryInfo(Application.persistentDataPath);
            dataDir.Delete(true);
        }

        [MenuItem("Dev/Data/Clean PlayerPrefs", priority = 11)]
        private static void CleanPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }

        [MenuItem("Dev/Data/Open Persistant Data Path", priority = 41)]
        private static void OpenPersistantDataPath()
        {
            EditorUtility.RevealInFinder(Application.persistentDataPath);
        }
    }
}