using System;
using TMPro;
using UnityEngine;

namespace AlexKadyrbayev.Utilities
{
    public class ColliderButton : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer m_SpriteRenderer;
        [SerializeField] private TextMeshPro m_Text;
        [SerializeField] private Color m_ColorOnDowned;
        [SerializeField] private Color m_BaseButtonColor;
        [SerializeField] private Color m_UninteractableButtonColor;
        [SerializeField] private Color m_UninteractableTextColor;
        [SerializeField] private Color m_BaseTextColor;

        [SerializeField]
        private bool m_IsInteractable = true;

        public Action OnClick;

        public void SetInteractable(bool isInteractable)
        {
            m_IsInteractable = isInteractable;


            if (!isInteractable)
            {
                m_Text.color = m_UninteractableTextColor;
                m_SpriteRenderer.color = m_UninteractableButtonColor;
            }
            else
            {
                m_SpriteRenderer.color = m_BaseButtonColor;
                m_Text.color = m_BaseTextColor;
            }
        }

        public void SetEnable(bool status)
        {
            gameObject.SetActive(status);
        }

        private void OnMouseDown()
        {
            if (!m_IsInteractable)
            {
                return;
            }
            m_SpriteRenderer.color = m_ColorOnDowned;
        }

        private void OnMouseUp()
        {
            if (!m_IsInteractable)
            {
                return;
            }
            m_SpriteRenderer.color = m_BaseButtonColor;
            OnClick?.Invoke();
        }

        public void SetText(string text)
        {
            m_Text.text = text;
        }

        public void SetTextColor(Color color)
        {
            m_Text.color = color;
        }

        public void SetButtonColor(Color color)
        {
            m_SpriteRenderer.color = color;
        }
    }
}