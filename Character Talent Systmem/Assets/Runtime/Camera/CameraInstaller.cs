using UnityEngine;
using Zenject;

public class CameraInstaller : MonoInstaller
{
    [SerializeField] private PanAndZoomCamera m_Camera;

    public override void InstallBindings()
    {
        Container.Bind<PanAndZoomCamera>()
            .FromInstance(m_Camera)
            .AsSingle();
    }
}
